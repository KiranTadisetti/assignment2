import React, { Component } from 'react';
import {BrowserRouter} from 'react-router-dom'

import Page from './containers/Page/Page'

class App extends Component {
 
  render () {
    return (

      <BrowserRouter>
      <div className="App">
        <Page />
      </div>
      </BrowserRouter>
    );
  }
}

export default App;
