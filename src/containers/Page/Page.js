import React, { Component } from 'react';
import {Route,Switch,NavLink} from 'react-router-dom'

import Courses from '../Courses/Courses';
import Users from '../Users/Users';
import Course from '../Course/Course';



class Page extends Component {
    render() {
        return (
            <div>

            <nav>
                <ui>
                    <li>
                        <NavLink to="/courses">All Courses</NavLink>
                    </li>
                    <li>
                        <NavLink to="/users">Users</NavLink>
                    </li>
                </ui>
            </nav>
                <Switch>
                    <Route path={["/courses","/all-courses"]} exact component={Courses}/>
                    <Route path="/users" exact component={Users}/>
                    <Route path="/course/:id/:name" exact component={Course}/>
                    
                    <Route render={()=><h1> Not Found!!</h1>}/>
                    
                </Switch>
            </div>
        )
    }
}

export default Page;