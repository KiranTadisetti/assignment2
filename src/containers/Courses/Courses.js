import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';

import Course from '../Course/Course'
import './Courses.css';

class Courses extends Component {
    state = {
        courses: [
            { id: 1, title: 'Angular - The Complete Guide' },
            { id: 2, title: 'Vue - The Complete Guide' },
            { id: 3, title: 'PWA - The Complete Guide' }
        ],
        courseSelectedId:null,
        courseName:null
    }

    courseSelectedHandler = (id,coursename) => {
        this.setState({courseSelectedId:id,courseName:coursename})
       // alert(id)
       
    }
    render () {
        let course=this.state.courseSelectedId?<Redirect to={`/course/${this.state.courseSelectedId}/${this.state.courseName}`}/>:null;
        return (
            <div>
                {course}
                <h1>Amazing Udemy Courses</h1>
                <section className="Courses">
                    {
                        this.state.courses.map( course => {
                            return <article className="Course" key={course.id} onClick={()=>this.courseSelectedHandler(course.id,course.title)}>{course.title} </article>;
                        } )
                    }
                </section>
                
            </div>
        );
    }
}

export default Courses;