import React, { Component } from 'react';

class Course extends Component {
    state={
        courseTitle:null,
        courseID:null
    }
    componentDidMount(){
        if(this.props.match.params){
            if(this.props.match.params.id && this.props.match.params.name){
                this.setState({courseID:this.props.match.params.id,courseTitle:this.props.match.params.name})
            }
        }
    }
    render () {
        return (
            <div>
                <h1>{this.state.courseTitle}</h1>
                <p>You selected the Course with ID: {this.state.courseID}</p>
            </div>
        );
    }
}

export default Course;